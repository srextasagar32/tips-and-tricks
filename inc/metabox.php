<?php 
function add_simple_metaboxes(){
	add_meta_box( 'simple_text', __( 'Simple Text', 'tips-and-tricks' ), 'simple_callback', 'post', 'side', 'default' );
}
add_action( 'add_meta_boxes','add_simple_metaboxes' );

function simple_callback(){
	global $post;

	wp_nonce_field( basename( __FILE__ ), 'simple_text_nonce' );

	$simple_text = get_post_meta( $post->ID,'_simple_text', true ); ?>
	<div class="simplesttruc">
		<input type="text" name="simple_text" value="<?php echo $simple_text; ?>" class="widefat"/>
	</div>
	<?php
}

function simple_save( $post_id ){

	if ( !isset( $_POST['simple_text_nonce'] ) || !wp_verify_nonce( $_POST['simple_text_nonce'], basename( __FILE__ ) ) ){
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ){
		return;
	}
	  // Sanitize user input.
    $simple_text = sanitize_text_field( $_POST['simple_text'] );

	// Update the meta field in the database.
  	update_post_meta( $post_id, '_simple_text', $simple_text);
}
add_action( 'save_post','simple_save', 1, 2  );
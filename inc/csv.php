<?php
// Set your CSV feed
$feed = get_template_directory_uri().'/csv/staffs.csv';

$keys = array();
$newArray = array();

// Function to convert CSV into associative array
function csvToArray($file, $delimiter) { 
  if (($handle = fopen($file, 'r')) !== FALSE) { 
    $i = 0; 
    while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) { 
      for ($j = 0; $j < count($lineArray); $j++) { 
        $arr[$i][$j] = $lineArray[$j]; 
      } 
      $i++; 
    } 
    fclose($handle); 
  } 
  return $arr; 
} 


// Do it
$data = csvToArray( $feed, ',' );


// Set number of elements (minus 1 because we shift off the first row)
$count = count($data) - 1;

//Use first row for names  
$labels = array_shift($data);

foreach ($labels as $label) {
	$label = sanitize_title($label);
    $keys[] = $label;
}
// Add Ids, just in case we want them later
$keys[] = 'id';
for ($i = 0; $i < $count; $i++) {
  $data[$i][] = $i;
}
 
// Bring it all together
for ($j = 0; $j < $count; $j++) {
  $d = array_combine($keys, $data[$j]);
  $newArray[$j] = $d;
}

foreach( $newArray as $arrays ){

}

// Create post object
// $my_post = array(
//   'post_title'    => wp_strip_all_tags( $_POST['post_title'] ),
//   'post_content'  => $_POST['post_content'],
//   'post_status'   => 'publish',
//   'post_author'   => 1,
//   'post_category' => array( 8,39 )
// );
// wp_insert_post( $my_post );
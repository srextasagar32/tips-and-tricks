<?php
/**
 * Template Name: Practice
 *
 * @package Tips and Tricks
 */
session_start();
get_header();
   
   if( isset( $_SESSION['counter'] ) ) {
      $_SESSION['counter'] += 1;
   }else {
      $_SESSION['counter'] = 1;
   }
	
   $msg = "You have visited this page ".  $_SESSION['counter'];
   $msg .= "in this session.";
?>
 <div>
      <h3>Setting up a PHP session</h3>
   </div>
   
   <article>
      <?php  echo ( $msg ); ?>
   </article>
<?php
get_footer();